import React from 'react';
import Navbar from './Navbar';
import AttendeesList from './AttendeesList';

class AttendConferenceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      conferenceList: [], 
      conference: "", 
      name: "", 
      email: "", 
    };
  }
  

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      conference: this.state.conference, 
      name: this.state.name, 
      email: this.state.email 
    };

    const attUrl = 'http://localhost:8001/api/attendees/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(attUrl, fetchConfig);
    if (response.ok) {
      const newAttendee = await response.json();
      // console.log(newAttendee);

      const cleared = {
        conference: "", 
        name: "", 
        email: "", 
      };
      this.setState(cleared);
      }
    }
 

  handleNameChange = (event) => {
    const value = event.target.value;
    this.setState({name: value})
  }

  handleEmailChange = (event) => {
    const value = event.target.value;
    this.setState({email: value})
  }

  handleConferenceChange = (event) => {
    const value = event.target.value;
    this.setState({conference: value})
  }


  async componentDidMount() {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);
    // console.log("response: ", response);
    if (response.ok) {
      const data = await response.json();
      // console.log("data: ", data);
      this.setState({conferenceList: data.conferences});
      // console.log("DidMount: ", data.conferenceList);
      }
  }


  render() {
    return (
      <div className="col">
      <div className="card shadow">
        <div className="card-body">
          <form onSubmit={this.handleSubmit} id="create-attendee-form">
            <h1 className="card-title">It's Conference Time!</h1>
            <p className="mb-3">
              Please choose which conference
              you'd like to attend.
            </p>
            {/* <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
              <div className="spinner-grow text-secondary" role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            </div> */}
            <div className="mb-3">

              <select onChange={this.handleConferenceChange} 
                required name="conference" id="conference" value={this.state.conference}
                className="form-select">
                <option value="">Choose a conference</option>
                {/* {console.log("render conferenceList: ", this.state.conferenceList)} 
                {console.log("render conference: ", this.state.conference)}  */}
                {this.state.conferenceList.map(conference => {
                  return (
                    <option key={conference.href} value={conference.href}>
                      {conference.name}
                    </option>
                  );
                })}
              </select>

            </div>
            <p className="mb-3">
              Now, tell us about yourself.
            </p>
            <div className="row">
              <div className="col">
                <div className="form-floating mb-3">
                  <input onChange={this.handleNameChange} required placeholder="Your full name" 
                  type="text" id="name" name="name" value={this.state.name}
                  className="form-control" /> 
                  <label htmlFor="name">Your full name</label>
                </div>
              </div>
              <div className="col">
                <div className="form-floating mb-3">
                  <input onChange={this.handleEmailChange} required placeholder="Your email address" 
                  type="email" id="email" name="email" value={this.state.email}
                  className="form-control" />
                  <label htmlFor="email">Your email address</label>
                </div>
              </div>
            </div>
            <button className="btn btn-lg btn-primary">I'm going!</button>
          </form>
          <div className="alert alert-success d-none mb-0" id="success-message">
            Congratulations! You're all signed up!
          </div>
        </div>
      </div>
    </div>
    );
  }
};

export default AttendConferenceForm;      