import React from 'react';
import Navbar from './Navbar';
import AttendeesList from './AttendeesList';


class LocationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "", 
      city: "", 
      state: "", 
      room_count: 0, 
      states: [], 
    };
  }
  
  
  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name: this.state.name, 
      room_count: this.state.room_count, 
      city: this.state.city, 
      state: this.state.state
    };


    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);

      const cleared = {
        name: '',
        room_count: '',
        city: '',
        state: '',
      };
      this.setState(cleared);
      }
    }
 

  handleNameChange = (event) => {
    const value = event.target.value;
    this.setState({name: value})
  }

  handleRoomChange = (event) => {
    const value = event.target.value;
    this.setState({room_count: value})
  }

  handleCityChange = (event) => {
    const value = event.target.value;
    this.setState({city: value})
  }

  handleStateChange = (event) => {
    const value = event.target.value;
    this.setState({state: value})
  }

  // handleNameChange(event) {
  //   const value = event.target.value;
  //   this.setState({name: value})
  // }

  async componentDidMount() {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({states: data.states});
      }
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            {/* <form id="create-location-form"> */}
            <form onSubmit={this.handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                {/* <input placeholder="Name" required type="text" name="name" id="name" className="form-control"/> */}
                <input onChange={this.handleNameChange} placeholder="Name" required
                   type="text" name="name" id="name" value={this.state.name}
                   className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleRoomChange} placeholder="Room count" 
                required type="number" name="room_count" id="room_count" value={this.state.room_count}
                className="form-control"/>
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleCityChange} placeholder="City" 
                required type="text" name="city" id="city" value={this.state.city}
                className="form-control"/>
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
              <select onChange={this.handleStateChange} 
                required name="state" id="state" value={this.state.state}
                className="form-select">
                <option value="">Choose a state</option>
                {this.state.states.map(state => {
                  return (
                    <option key={state.abbreviation} value={state.abbreviation}>
                      {state.name}
                    </option>
                  );
                })}
              </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
};

export default LocationForm;