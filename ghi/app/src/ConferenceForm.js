import React from 'react';
import Navbar from './Navbar';
import AttendeesList from './AttendeesList';

class ConferenceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "", 
      starts: "", 
      ends: "", 
      description: "", 
      max_presentations: "", 
      max_attendees: "" ,
      location: "", 
      locations: [], 
    };
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    // const data = {...this.state};
    const data = {
      name: this.state.name, 
      starts: this.state.starts, 
      ends: this.state.ends, 
      description: this.state.description, 
      max_presentations: this.state.max_presentations, 
      max_attendees: this.state.max_attendees, 
      location: this.state.location,
    };


    const ConferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(ConferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);
      const cleared = {
        name: "", 
        starts: "", 
        ends: "", 
        description: "", 
        max_presentations: "", 
        max_attendees: "" ,
        location: "", 
      };
      this.setState(cleared);
    }
  }
 

  handleNameChange = (event) => {
    const value = event.target.value;
    this.setState({name: value})
  }

  handleStartsChange = (event) => {
    const value = event.target.value;
    this.setState({starts: value})
  }

  handleEndsChange = (event) => {
    const value = event.target.value;
    this.setState({ends: value})
  }

  handleDescriptionChange = (event) => {
    const value = event.target.value;
    this.setState({description: value})
  }

  handleMaxPresentationsChange = (event) => {
    const value = event.target.value;
    this.setState({max_presentations: value})
  }

  handleMaxAttendeesChange = (event) => {
    const value = event.target.value;
    this.setState({max_attendees: value})
  }

  handleLocationChange = (event) => {
    const value = event.target.value;
    this.setState({location: value})
  }

  async componentDidMount() {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({locations: data.locations});
    }
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
              <input onChange={this.handleNameChange} placeholder="Name" 
                required type="text" name="name" id="name" value={this.state.name}
                className="form-control" />
              <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleStartsChange} placeholder="Starts" 
                required type="date" name="starts" id="starts" value={this.state.starts}
                className="form-control" />
                <label htmlFor="starts">starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleEndsChange} placeholder="Ends" 
                required type="date" name="ends" id="ends" value={this.state.ends}
                className="form-control" />
                <label htmlFor="ends">ends</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleDescriptionChange} placeholder="Description" 
                required type="text" name="description" id="description" value={this.state.description}
                className="form-control" />
                <label htmlFor="description">Description</label>

              </div>     
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxPresentationsChange} placeholder="Max_presentations" 
                required type="number" name="max_presentations" id="max_presentations" value={this.state.max_presentations}
                className="form-control" />
                <label htmlFor="max_presentations">Max Presentations</label>
              </div>  
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxAttendeesChange} placeholder="Max_attendees" 
                required type="number" name="max_attendees" id="max_attendees" value={this.state.max_attendees}
                className="form-control" />
                <label htmlFor="min_presentations">Max Attendees</label>
              </div>                                     
              <div className="mb-3">
              <select onChange={this.handleLocationChange} 
                required name="location" id="location" value={this.state.location}
                className="form-select">
                <option value="">Choose a location</option>
                {/* {console.log(this.state.locations)} */}
                {this.state.locations.map(location => {
                   return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  );
                })}

              </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      );
  }
};

export default ConferenceForm;