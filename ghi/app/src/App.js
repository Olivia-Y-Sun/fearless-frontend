
import React from 'react';
import Navbar, { NavLink } from './Navbar';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { Link, Outlet } from 'react-router-dom';
import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';



function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <BrowserRouter>
      <Navbar />
      <div className="container">
      <Routes>
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/conferences/new" element={<ConferenceForm />} />
          <Route path="/attendees/new" element={<AttendConferenceForm />} />
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/attendees" element={<AttendeesList />} />
          <Route path="/PresentationForm" element={<PresentationForm />} />
          <Route path="" element={<MainPage />} />
          {/* <Route index element={<MainPage />} /> */}

        </Routes>
      </div>
    </BrowserRouter>
    </>
  );
}

export default App;

