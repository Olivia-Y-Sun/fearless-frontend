import React from "react";

const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <a className="navbar-brand" href="#">
        Conference GO
      </a>
      <div className="collapse navbar-collapse" id="navbarNavDropdown">
        <ul className="navbar-nav">
          <li className="nav-item active">
            <a className="nav-link" href="http://localhost:3000/">
              Home 
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="http://localhost:3000/new-location.html">
              New location
            </a>
            {/* <NavLink className="nav-link" aria-current="page" to="/locations/new">
              New location
            </NavLink> */}
          </li>
          <li className="nav-item">
            <a className="nav-link" href="http://localhost:3000/new-conference.html">
              New Conference
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="http://localhost:3000/new-presentation.html">
              New Presentation
            </a>
          </li>          
          <li className="nav-item dropdown">
            <a
              className="nav-link dropdown-toggle"
              href="#"
              id="navbarDropdownMenuLink"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              Dropdown link
            </a>
            <div
              className="dropdown-menu"
              aria-labelledby="navbarDropdownMenuLink"
            >
              <a className="dropdown-item" href="#">
                Action
              </a>
              <a className="dropdown-item" href="#">
                Another action
              </a>
              <a className="dropdown-item" href="#">
                Something else here
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;