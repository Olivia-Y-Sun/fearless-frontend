import React, { Component } from 'react'
import Navbar from './Navbar';
import AttendeesList from './AttendeesList';

class PresentationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      presenter_name: "", 
      presenter_email: "", 
      company_name: "", 
      title: "", 
      synopsis: "", 
      conference: "" ,
      conferenceList: [], 
    };
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      presenter_name: this.state.presenter_name, 
      presenter_email: this.state.presenter_email, 
      company_name: this.state.company_name, 
      title: this.state.title, 
      synopsis: this.state.synopsis, 
      conference: this.state.conference, 
    };

    const selectTag = document.getElementById('conference');
    const conferenceId = selectTag.options[selectTag.selectedIndex].value;
    const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      // console.log(newAttendee);

      const cleared = {
        presenter_name: "", 
        presenter_email: "", 
        company_name: "", 
        title: "", 
        synopsis: "", 
        conference: "" ,
      };
      this.setState(cleared);
      }
    }
 

  handleNameChange = (event) => {
    const value = event.target.value;
    this.setState({name: value})
  }

  handleEmailChange = (event) => {
    const value = event.target.value;
    this.setState({email: value})
  }

  handleCompanyChange = (event) => {
    const value = event.target.value;
    this.setState({company_name: value})
  }

  handleTitleChange = (event) => {
    const value = event.target.value;
    this.setState({title: value})
  }

  handleSynopsisChange = (event) => {
    const value = event.target.value;
    this.setState({synopsis: value})
  }

  handleConferenceChange = (event) => {
    const value = event.target.value;
    this.setState({conference: value})
  }


  async componentDidMount() {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);
    // console.log("response: ", response);
    if (response.ok) {
      const data = await response.json();
      // console.log("data: ", data);
      this.setState({conferenceList: data.conferences});
      // console.log("DidMount: ", data.conferenceList);
      }
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={this.handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} placeholder="Name" 
                required type="text" name="presenter_name" id="presenter_name" value={this.state.presenter_name}
                className="form-control" />
                <label for="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleEmailChange} placeholder="Presenter email" 
                required type="email" name="presenter_email" id="presenter_email" value={this.state.presenter_email}
                className="form-control" />
                <label for="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleCompanyChange} placeholder="Company name" 
                type="text" name="company_name" id="company_name" value={this.state.company_name}
                className="form-control" />
                <label for="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleTitleChange} placeholder="Title" 
                required type="text" name="title" id="title" value={this.state.title}
                className="form-control" />
                <label for="title">Title</label>
              </div>
              <div className="mb-3">
                <label for="synopsis">Synopsis</label>
                <textarea onChange={this.handleSynopsisChange} id="synopsis" rows="3" name="synopsis" value={this.state.synopsis}
                className="form-control" ></textarea>
              </div>
              <div className="mb-3">
                <select onChange={this.handleConferenceChange} 
                required name="conference" id="conference" value={this.state.conference}
                className="form-select"> 
                  <option value="">Choose a conference</option>
                {/* {console.log("render conferenceList: ", this.state.conferenceList)} 
                {console.log("render conference: ", this.state.conference)}  */}
                {this.state.conferenceList.map(conference => {
                  return (
                    <option key={conference.href} value={conference.href}>
                      {conference.name}
                      </option>
                  );
                })}
              </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      );
  }
};
 
export default PresentationForm;



