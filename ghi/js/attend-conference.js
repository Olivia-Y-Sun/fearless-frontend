window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
  
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    try {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
  
      // Here, add the 'd-none' class to the loading icon
      const selectTag_1 = document.getElementById('loading-conference-spinner');
      selectTag_1.classList.add("d-none");
      // Remove the 'd-none' class from the select tag  
      selectTag.classList.remove("d-none");
      const selectTag_2 = document.getElementById('success-message');
      selectTag_2.classList.remove("d-none");
      
      // const selectTag_2 = document.getElementById('success_message');
      // attendConForm.classlist.add('d-none')
      // selectTag_2.classList.remove('d-none')
  
      // Get the attendee form element by its id
      const attendConForm = document.getElementById('create-attendee-form');
      // Add an event handler for the submit event
      attendConForm.addEventListener('submit', async event => {
      // Prevent the default from happening
      event.preventDefault();
      // Create a FormData object from the form
      const formData = new FormData(attendConForm);
      // Get a new object from the form data's entries
      const attendConInfo = Object.fromEntries(formData);
      // console.log(attendConInfo);
      // json.loads
      try {
          const resp = await fetch("http://localhost:8001/api/attendees/", {
          method: 'post',
          body: JSON.stringify(attendConInfo),
          headers: {
              'Content-Type': 'application/json'
          }
      });
  
      if (resp.ok) {
          attendConForm.reset();
          const getSuccessMessage = document.getElementById("success-message");
          attendConForm.classList.add('d-none');
          getSuccessMessage.classList.remove('d-none');
          // const data = await resp.json();
          // console.log(data);
  
      } else {
          console.log("What's wrong?")
      }
  
      } catch (e) {
          console.log(e);
          }
      });
  } catch (err) {
  console.log(err);
  }    
  });
  