
window.addEventListener('DOMContentLoaded', async ()=> {
  try {
      const response = await fetch('http://localhost:8000/api/locations/');
      const { locations } = await response.json();
      const locationSelect = document.getElementById('location');
      //   console.log(conferences);
      locations.forEach(location => {
        //   console.log(conference);
          // console.log(location.id);
          locationSelect.innerHTML += `<option value="${location.id}">${location.name}</option>`;
      });

      const conferenceForm = document.getElementById('create-conference-form');
      conferenceForm.addEventListener('submit', async event => {
          event.preventDefault();
          const formData = new FormData(conferenceForm);
          const conferenceInfo = JSON.stringify(Object.fromEntries(formData));
          //json.loads
          // console.log(conferenceInfo);
          try {
              const resp = await fetch("http://localhost:8000/api/conferences/", {
              method: 'post',
              // body: JSON.stringify(conferenceInfo),
              body: conferenceInfo,
              headers: {
                  'Content-Type': 'application/json'
              }
          });
          if (resp.ok) {
              conferenceForm.reset();
              const data = await resp.json();
          } else {
            console.log("what's wrong?")
          }


          } catch (e) {
              console.log(e);
            }
      });
  } catch (err) {
      console.log("err");
  }    
});