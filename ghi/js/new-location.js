// function createLocation(name, room_count, city, state) {
//     const locationContainer = document.querySelector("#display");
//     locationContainer.innerHTML = `
//     <div>${name}</div>
//     <div>${room_count}</div>
//     <div>${city}</div>
//     <div>${state}</div>
//     `;
// }

window.addEventListener('DOMContentLoaded', async ()=> {
    try {
        const response = await fetch('http://localhost:8000/api/states/');
        const { states } = await response.json();
        const stateSelect = document.getElementById('state');
        // console.log(states);
        states.forEach(state => {
            // console.log(state);
            stateSelect.innerHTML += `<option value="${state.abbreviation}">${state.name}</option>`;
        });

        const locationForm = document.getElementById('create-location-form');
        locationForm.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(locationForm);
            const locationInfo = Object.fromEntries(formData);
            //json.loads
            // console.log(locationInfo);
            try {
            const resp = await fetch("http://localhost:8000/api/locations/", {
                method: 'post',
                body: JSON.stringify(locationInfo),
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            if (resp.ok) {
                locationForm.reset();
                const data = await resp.json();
            } else {
                console.log("What's wrong?")
            }

            // const { name, room_count, city, state } = await resp.json();
            // createLocation(name, room_count, city, state);
        } catch (e) {
            console.log(e);
        }
        });
    } catch (err) {
        console.log(err);
    }    
});
