
function createCard(name, location, description, pictureUrl, starts, ends) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-title">${location}</h6>
          <p class="card-text">${description}</p>
          <p class="card-text">${starts} - ${ends}</p>
        </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
      
      if (!response.ok) {
        // Figure out what to do when the response is bad
        return null;
      } else {
        const data = await response.json();
        // console.log(data);
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          // console.log(detailResponse);
          if (detailResponse.ok) {
            const details = await detailResponse.json();

            const name = details.conference.name;
            const location = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            // console.log(pictureUrl);

            const dateStarts = new Date(details.conference.starts);
            const starts= dateStarts.getMonth() + "/" + dateStarts.getDate() + "/" + dateStarts.getFullYear();
            const dateEnds = new Date(details.conference.ends);
            const ends= dateEnds.getMonth() + "/" + dateEnds.getDate() + "/" + dateEnds.getFullYear();


            const html = createCard(name, location, description, pictureUrl, starts, ends);
            const column = document.querySelector('.col');
            column.innerHTML += html;
            // console.log("test again", details);
          }
        }
  
      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.log(e);
      console.log("test");
    }
  
  });

 
// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';
  
//     try {
//       const response = await fetch(url);
  
//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//         return null;
//       } else {
//         const data = await response.json();
//         // get the first value from the array of that data structure, the one for the AI and Robotics Conference
//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML += conference.name;
  
//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//         //   const details = await detailResponse.json();
//         // //   console.log(details);
//         // //   console.log(details.conference.description);
//         //   const conferenceDetail = details.conference.description
//         //   const nameTagDetail = document.querySelector('.card-title');
//         //   nameTagDetail.innerHTML += conferenceDetail;

//             const details = await detailResponse.json();
//             const title = details.conference.title;
//             const description = details.conference.description;
//             const pictureUrl = details.conference.location.picture_url;
//             const html = createCard(title, description, pictureUrl);
//             const column = document.querySelector('.col');
//             column.innerHTML += html;

//         }
//         }
//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//       console.log(e);
//     }
  
//   });